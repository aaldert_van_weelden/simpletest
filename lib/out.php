<?php
class Out {
	/**
	 * Helper to print the content.
	 * CLI aware
	 * 
	 * @param string $in        	
	 */
	public static function print_line($in) {
		if (php_sapi_name () == 'cli') {
			print $in . "\n";
			return;
		}
		print $in . "<br>";
		return;
	}
	
	/**
	 * Helper to print the content underlined.
	 * CLI aware
	 *
	 * @param string $in
	 */
	public static function print_underline($in) {
		if (php_sapi_name () == 'cli') {
			print "\n\033[32m".$in."\033[0m\n";
			return;
		}
		print "<br><u>".$in."</u>:<br>";
		return;
	}
	
	/**
	 * Helper to dump the content to screen, can be expanded and collapsed.
	 * CLI aware
	 * 
	 * @param object $object        	
	 * @param string $caption        	
	 * @param boolean $halt        	
	 */
	public static function dump($object, $caption = null, $halt = false) {
		if (php_sapi_name () == 'cli') {
			var_dump ( $object );
			return;
		}
		
		$ID = self::generateRandomString ();
		print '<br>';
		print '<span style="display:visible;cursor:pointer;" id="plus_' . $ID . '" onclick="
			document.getElementById(\'plus_' . $ID . '\').style.display=\'none\';
			document.getElementById(\'minus_' . $ID . '\').style.display=\'inline\';
			document.getElementById(\'dump_' . $ID . '\').style.display=\'block\';
		">[+]&nbsp;</span>';
		print '<span style="display:none;cursor:pointer;" id="minus_' . $ID . '" onclick="
			document.getElementById(\'plus_' . $ID . '\').style.display=\'inline\';
			document.getElementById(\'minus_' . $ID . '\').style.display=\'none\';
			document.getElementById(\'dump_' . $ID . '\').style.display=\'none\';
		">[-]&nbsp;</span>';
		if ($caption != null) {
			print '<b>' . $caption . ' : </b><br>';
		}
		print '<span style="display:none;" class="util_dump" id="dump_' . $ID . '">';
		print '<pre>';
		var_dump ( $object );
		print '</pre>';
		print '</span>';
		if ($halt) {
			exit ();
		}
	}
	public static function generateRandomString($length = 5) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen ( $characters );
		$randomString = '';
		for($i = 0; $i < $length; $i ++) {
			$randomString .= $characters [rand ( 0, $charactersLength - 1 )];
		}
		return $randomString;
	}
	
	/**
	 * Helper to dump the content to screen, not collapsed.
	 * CLI aware
	 * 
	 * @param object $object        	
	 * @param string $caption        	
	 * @param boolean $halt        	
	 */
	public static function dumpAndShow($object, $caption = null, $halt = false) {
		if (php_sapi_name () == 'cli') {
			var_dump ( $object );
			return;
		}
		$ID = self::generateRandomString ();
		print '<br>';
		if ($caption != null) {
			print '<b>' . $caption . ' : </b><br>';
		}
		print '<pre>';
		var_dump ( $object );
		print '</pre>';
		if ($halt) {
			exit ();
		}
	}
	
	/**
	 * Print as a label.
	 * CLI aware
	 * 
	 * @param string $in        	
	 * @param string $var        	
	 */
	public static function label($in, $var) {
		if (php_sapi_name () == 'cli') {
			return;
		}
		$hr = "\n\n---------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
		var_dump ( $hr . "*** $in :", $var, $hr );
	}
}